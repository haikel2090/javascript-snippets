// rangeBitCount -> codeSignal
// 96 chars -> recursive solution

let a = 2, b = 7;

var rangeBitCount = s = (a, b, i = a, v = '') => 
  i > b 
    ? [...v].reduce((k, c) => k + +c, 0) 
    : (
        v += i.toString(2) , 
        s(a, b, i + 1, v)
      )

console.log(rangeBitCount(a, b, i = a, '')) // 11