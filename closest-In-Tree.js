// https://app.codesignal.com/challenge/vEA6MQdn8BuKrEaaT
// recursive solution 114 chars
m = 2**53
closestInTree = w = (t, n, r = n) => 
  t 
    ? (
      (v = Math.abs(n - t.value)) < m 
      && (
          m = v, 
          r = t.value
      ),
      w(t.value > n 
          ? t.left 
          : t.right, 
          n, 
          r
      )
    ) 
    : r

closestInTree(t, 2) // 1
closestInTree(t, 12) // 12
