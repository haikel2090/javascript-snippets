// https://app.codesignal.com/challenge/CqXTeBAbeYgeK6L27
// recursive solution -> 227 chars

m = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 }
i = 1
integerValueOfRomanNumeral = v = (s, r = m[s[0]]) => 
  i < s.length ?  
  (
    p = m[s[i-1]],
    c = m[s[i++]],
    v(s, r = p >= c ? r + c  : r - p * 2 + c)
  ) : d(s) ? r : -1

d = n => /^M{0,}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/.test(n)

console.log(integerValueOfRomanNumeral("MMXV")); // 2015
console.log(integerValueOfRomanNumeral("XLX")); // -1
