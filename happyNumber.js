// https://app.codesignal.com/challenge/cyDPaP4tuzgJnqTDb
function happyNumber(n) {
  if (n < 5) return n === 1;

  // 1^2 + 7^2 = 50....
  let v = ('' + n).split('').reduce((a, c) => a += c ** 2, 0);
  return happyNumber(v)
}

console.log(happyNumber(17)) // false
console.log(happyNumber(19)) // true
console.log(happyNumber(1111111)) // true