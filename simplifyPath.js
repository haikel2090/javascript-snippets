// https://app.codesignal.com/challenge/9AeCgehzor6jPN6ZK
function simplifyPath(path) {
   let p = path.split(/\/|\/\//g);
   let stack = [];
   
   for(let i = 0; i < p.length ; i++) {
       if(p[i] === '..') stack.pop()
       if(p[i] === '.' || p[i] === '') continue;
       if(/\w/gi.test(p[i])) stack.push(p[i])
   }
   
   return '/' + stack.join('/')
}


console.log(simplifyPath("/cHj/T//./v"))
console.log(simplifyPath("/home/a/./x/../b//c/"))

/* Short solution 122 chars
s = []
simplifyPath = p =>    
  p.split(/\/|\/\//g).map(v => 
    (v != '.' || !v) 
      & (v == '..' 
      && s.pop() || /\w/i.test(v) && s.push(v)))
       
  && '/'+ s.join('/')
*/
