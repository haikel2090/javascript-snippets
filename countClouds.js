// https://app.codesignal.com/challenge/u6ob2kqxxQpCRsHaP

var countClouds = function(s) {
    let n = 0;
    let l = s.length

    s.map((v, i) => {
        s[i].map((k, j) => {
            n += s[i][j] == '1' ? dfs(s, i, j, l) : 0
        });
    });
    return n
}

function dfs(skyMap, i, j) {
    if (
        i < 0 || i >= skyMap.length 
        || j >= skyMap[i].length || j < 0 
        || skyMap[i][j] === '0'
    ) 
        return 0;
            
    skyMap[i][j] = '0';
    dfs(skyMap, i + 1, j);
    dfs(skyMap, i - 1, j)
    dfs(skyMap, i, j + 1)
    dfs(skyMap, i, j - 1)
    return 1
}

console.log(countClouds([
    ['0', '1', '1', '0', '1'],
    ['0', '1', '1', '1', '1'],
    ['0', '0', '0', '0', '1'],
    ['1', '0', '0', '1', '1']
])); // 2

console.log(countClouds([
    ['0', '1', '0', '0', '1'],
    ['1', '1', '0', '0', '0'],
    ['0', '0', '1', '0', '1'],
    ['0', '0', '1', '1', '0'],
    ['1', '0', '1', '1', '0']
])); // 5
