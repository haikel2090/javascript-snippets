const map = new Map();

// store value: key - value
map.set(1, 'hello');
map.set(2, 'world');

// get value from Map
map.get(1) // 'hello'

// Iteration over Map
for (let [k, v] of map) {
  console.log(k, v);
}

console.log('-----');

// Another way Iteration over Map
map.forEach( (value, key, map) => {
  console.log(`${key}: ${value}`);
});

console.log('-----');

// Map from object
let obj = { name: "Mike",  age: 30 };

let mapFromObject = new Map(Object.entries(obj));

console.log( mapFromObject.get('name') ); // Mike