function bfs(root) {
  let queue = [];
  queue.push(root)

  while(queue.length>0) {
    root = queue.shift();

    if(root.left) {
      queue.push(root.left)
    }

    if(root.right) {
      queue.push(root.right)
    }

  }
}

console.log(bfs(root)) 
// [1, 2, 3, 4, 5, 6, 7]
