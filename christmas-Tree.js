// https://app.codesignal.com/challenge/Du97C9fPptK7kt7Tg

function drawCrown(level, height) {
  let res = [];
  let p = ' '.repeat(level+height) + '*';
  res.push(p);
  res.push(p);
  p = ' '.repeat(level+height-1) + '***';
  res.push(p);
  return res;
}

function drawLines(height, width, spaces) {    
  let res = [];

  while(height) {
    let p = ' '.repeat(spaces);
    p += '*'.repeat(width);
    res.push(p)
    width += 2;
    spaces--;
    height--;
  }
  return res;
}

function drawLevels(level, height) {
  let width = 5;
  let spaces = level + height - 2;

  let res= [];

  while(level) {
    let r = drawLines(height, width, spaces);
    res.push(...r)
    width += 2;
    spaces--;
    level--;
  }
  return res;
}
function draw(levelNum, height) {  
  // draw root
  let res= [...drawCrown(levelNum, height)];
  // draw Levels
  res.push(...drawLevels(levelNum, height));
  
  let spaces = ((res[res.length-1].length - height)/2) | 0;
  // drawFoot
  while(levelNum) {
    let p = ' '.repeat(spaces);
    p += '*'.repeat(height % 2 > 0 ? height : height +1);
    res.push(p)
    levelNum--;
  }
  
  return res;
}

console.log(draw(2,4))
//console.log(draw(1,3))
//console.log(draw(4,8))
//console.log(draw(8,5))
//console.log(draw(3,3))
//console.log(draw(4,6))
