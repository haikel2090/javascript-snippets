// https://leetcode.com/problems/kth-smallest-element-in-a-bst/
var kthSmallest = function(root, k) {
    let result = [0,0];
    traverse(root, k , result);
    return result[1];
};

function traverse(root, k , result){
    if(root === null) { return; } // stop recursion if root is null else we start search for the Kth
	
    
    traverse(root.left, k , result); // we start traversing left
	
    if(++result[0] === k) {  // ++result[0] -> the same like -> int count =0; ++count;
	
        result[1] = root.val; // if we found Kth, we put it into a variable
		
        return; // stop recursion
    }
	
    // if we dont found the Kth in left, we start traversing right
    traverse(root.right, k , result);
}