// https://leetcode.com/problems/number-of-steps-to-reduce-a-number-to-zero/

var numberOfSteps  = function(num, cnt = 0) {
    
	if (num < 1) return cnt; // if num === zero, return the counter

	return num % 2 === 0 
		? numberOfSteps(num / 2, cnt + 1) // if num is even divide by 2 and increment counter by 1
		: numberOfSteps(num - 1, cnt + 1); // if num is odd subtract by 1 and increment counter by 1
};

console.log(numberOfSteps(14)) // 6
console.log(numberOfSteps(8)) // 4