// https://leetcode.com/problems/implement-trie-prefix-tree/
class Node {
  constructor (char) {
    this.char = char;
    this.children = {};
  }
}

var Trie = function() {
     this.root = new Node('^');
};

Trie.prototype.insert = function(word) {
    let currNode = this.root;
  for (let letter of word) {
    if (!currNode.children[letter])
      currNode = currNode.children[letter] = new Node(letter);
    else
      currNode = currNode.children[letter];
  }
  return currNode.children['$'] = '$';
};

Trie.prototype.search = function(word, prefix) {
    let currNode = this.root;

  for (const letter of word) {
    if(!currNode.children[letter]) return false
    else {
      currNode = currNode.children[letter];
    }
  }
 return currNode.children && prefix ? true : currNode.children['$'] === '$';
};


Trie.prototype.startsWith = function(prefix) {
      return this.search(prefix, true);
};

// Test cases
let trie = new Trie();

trie.insert("apple");
console.log(trie.search("apple"));   // returns true
console.log(trie.search("app"));     // returns false
console.log(trie.startsWith("app")); // returns true
trie.insert("app");   
console.log(trie.search("app"));     // returns true
