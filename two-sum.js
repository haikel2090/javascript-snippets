// Determine whether there is a pair of numbers, 
// where one number is taken from a and the other from b, 
// that can be added together to get a sum of v (boolean)
// Asked by Google
var twoSum = function (a, b, target) {
  let stack = b.slice(0);
  let isFound = false;

  while (stack.length > 0) {
    let item = stack.pop();
    let rest = target - item;
    console.log(item)

    if (a.indexOf(rest) > -1) {
      isFound = true;
      break;
    }
  }
  return isFound;
}

// test cases
console.log(twoSum([1, 2, 3], [10, 40, 30, 20], 42)) // true
console.log(twoSum([1, 2, 3], [10, 50, 30, 20], 42)) // false
console.log(twoSum([10, 2, 3], [10, 20, 30, 40], 40)) // true
