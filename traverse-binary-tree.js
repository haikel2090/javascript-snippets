/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var preorderTraversal = function(root) {
    
    let results = [];
    recusive(root, results)
    // return traverse(root); 
    return results;
};

// iteration solution pre-order
function traverse(root) {
  if(!root){ return []; }
    
  let stack = [];
  let results = [];

  stack.push(root)

  while (stack.length > 0) {
      
    root = stack.pop();    
      
    if(root) results.push(root.val);
    
    if(root.right) {
       stack.push(root.right)
    }
      
    if(root.left) {      
       stack.push(root.left)
    }           
  }

  return results
}

// ----------------------------------------------------------
// recusive solution pre-order
function recusive(root, results) {
    
    results.push(root.val)
    
    if(root.left === null && root.right === null) {
        return;
    }
    
    if(root.right) { recusive(root.right, results); }
    if(root.left) { recusive(root.left, results); }    
}