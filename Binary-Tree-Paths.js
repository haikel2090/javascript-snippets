// -> https://leetcode.com/problems/binary-tree-paths/
/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
var binaryTreePaths = function(root) {
    let results = [];
    traverse(root, '', results);
    return results
};

function traverse(root, path, results) {
    
    path += root.val;
    
    if(root.left === null && root.right === null) {
        results.push(path)
        return;
    }
    
    if(root.left) { traverse(root.left, path + '->' , results); }
    if(root.right) { traverse(root.right, path + '->' , results); }
}

// ["1->2->5", "1->3"]
