// 42. Trapping Rain Water
// https://leetcode.com/problems/trapping-rain-water/
function trap (height) {
    let i = 0, j = height.length-1; let leftMax = 0, rightMax = 0 , result = 0;

    while(i <= j){

      height[i] > left ? leftMax = height[i] : v += leftMax - height[i]
      height[j] > rightMax ? rightMax = height[j] : v += rightMax - height[j]

      result > leftMax ? i++ : j--;
    }
    return result; 
}

// Test cases
console.log(trap([ 0,1,0,2,1,0,1,3,2,1,2,1 ])) // 6
console.log(trap([ 1, 0, 1 ])) // 1
console.log(trap([ 1, 0, 1, 0, 2, 3 ])) // 2
console.log(trap([ 10, 6, 5, 0, 7, 2, 9, 11, 25, 3 ])) // 31
