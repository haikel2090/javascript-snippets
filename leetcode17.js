// 17. Letter Combinations of a Phone Number
// https://leetcode.com/problems/letter-combinations-of-a-phone-number/
const letterCombinations = function(digits) {
  if (digits.length === 0) return []
  const letters = {
    '2': 'abc',
    '3': 'def', 
    '4': 'ghi', 
    '5': 'jkl',
    '6': 'mno',
    '7': 'pqrs', 
    '8': 'tuv', 
    '9': 'wxyz'
  }

  const result = [];

  function walk(i, path) {
    if (i === digits.length) {
      result.push(path)
    } else {
      letters[digits[i]].split('').forEach(letter => {        
        walk(i + 1, path + letter);
      });            
    }
  }

  walk(0, '');
  return result;
}

console.log(letterCombinations('23'));