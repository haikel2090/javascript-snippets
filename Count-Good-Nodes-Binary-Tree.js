// https://leetcode.com/problems/count-good-nodes-in-binary-tree
var goodNodes = function(root) {
    
    if( root === null) return 0
    
    let count = 0;

    function helper(root, max) {
        if(root == null) return ;
        
        if(root.val >= max) {
            count++;
            max= root.val;
        }
        
        if(root.left !== null) helper(root.left, max);
        if(root.right !== null) helper(root.right, max);
    }
    
    helper(root, root.val)
    return count
};

// Test cases
[[3,1,4,3,null,1,5] // 4
[3,3,null,4,2] // 3
[2,4,4,4,null,1,3,null,null,5,null,null,null,null,5,4,4] // 6