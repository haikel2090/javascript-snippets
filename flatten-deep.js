// faltten array deep using DFS and recursion
let arr = [1, 2, 3, [4, 5, [6]], [7, 8], 9]

function flatt(arr) {
  let result = []
  const queue = arr.slice(0)
  while (queue.length > 0) {
    let item = queue.shift()
    if (Array.isArray(item)) {
      result.push(...flatt(item))
    } else {
      result.push(item)
    }
  }
  return result
}

console.log(flatt(arr))
// [1,2,3,4,5,6,7,8,9]